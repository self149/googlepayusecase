package com.onlinePayment.exception;

import org.springframework.http.ResponseEntity;

import com.onlinePayment.dto.TransactionSuccessResponseDto;

public class TransferAmountException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransferAmountException(ResponseEntity<TransactionSuccessResponseDto> transactionSuccess) {
		
		super(transactionSuccess.getBody().toString());
	}

}
