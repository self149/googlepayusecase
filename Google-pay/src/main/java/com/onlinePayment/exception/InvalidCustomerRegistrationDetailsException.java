package com.onlinePayment.exception;

import org.springframework.http.ResponseEntity;

import com.onlinePayment.dto.CustomerValidationWithPhoneNumberResponseDto;

public class InvalidCustomerRegistrationDetailsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidCustomerRegistrationDetailsException(String errorMsg,
			ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidation) {		
		super(errorMsg);		
	}

	
}
