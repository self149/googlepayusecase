package com.onlinePayment.exception;

public class CustomerNotRegisteredWithGpayAppException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotRegisteredWithGpayAppException(String errorMsg) {
		super(errorMsg);
	}

}
