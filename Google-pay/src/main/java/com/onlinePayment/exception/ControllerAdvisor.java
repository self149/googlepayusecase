package com.onlinePayment.exception;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import feign.FeignException;


@ControllerAdvice
public class ControllerAdvisor {

	Logger logger = LoggerFactory.getLogger(ControllerAdvisor.class);
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(errors);
    }
	
	
	@ExceptionHandler(SQLException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> sqlException(SQLException sqlException) {
		logger.error("SQL Exception with the exception msg: " + sqlException.getMessage());
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(sqlException.getMessage());
	}
	
	@ExceptionHandler(InvalidCustomerRegistrationDetailsException.class)
	public ResponseEntity<String> invalidCustomerRegistrationDetailsException(InvalidCustomerRegistrationDetailsException invalidCustomerException) {
		logger.error("InvalidCustomer Exception with the exception msg: " + invalidCustomerException.getMessage());
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(invalidCustomerException.getMessage());
	}
	@ExceptionHandler(TransferAmountException.class)
	public ResponseEntity<String> transferAmountException(TransferAmountException transferAmountException) {
		logger.error("Exception occured while transferring the amount with the exception msg: " + transferAmountException.getMessage());
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(transferAmountException.getMessage());
	}
	
	@ExceptionHandler(CustomerNotRegisteredWithGpayAppException.class)
	public ResponseEntity<String> customerNotRegisteredException(CustomerNotRegisteredWithGpayAppException unregisteredCustomerException) {
		logger.error("Exception occured while transferring the amount with the exception msg: " + unregisteredCustomerException.getMessage());
		return ResponseEntity
		        .status(HttpStatus.BAD_REQUEST)
		        .body(unregisteredCustomerException.getMessage());
	}
	
	@ExceptionHandler(FeignException.class)	
	public ResponseEntity<String> externalServiceCallException(FeignException feignException) {
		
		
		HttpStatus status = HttpStatus.resolve(feignException.status());
		if(status == null)
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		
		return ResponseEntity
		        .status(status)
		        .body(feignException.getMessage());
	}

}
