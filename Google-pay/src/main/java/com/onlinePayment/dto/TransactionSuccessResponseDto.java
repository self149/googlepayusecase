package com.onlinePayment.dto;

public class TransactionSuccessResponseDto {
	
	Long debitTransactionID;
	Long creditTransactionID;
	
	
	public TransactionSuccessResponseDto(Long debitTransactionID, Long creditTransactionID) {
		super();
		this.debitTransactionID = debitTransactionID;
		this.creditTransactionID = creditTransactionID;
	}
	public Long getDebitTransactionID() {
		return debitTransactionID;
	}
	public Long getCreditTransactionID() {
		return creditTransactionID;
	}
	public void setDebitTransactionID(Long debitTransactionID) {
		this.debitTransactionID = debitTransactionID;
	}
	public void setCreditTransactionID(Long creditTransactionID) {
		this.creditTransactionID = creditTransactionID;
	}
	public TransactionSuccessResponseDto() {
		super();
	}
	
	

}
