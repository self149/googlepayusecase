package com.onlinePayment.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class CustomerRegistrationDto {
	
    @NotBlank(message = "First name is a mandatory field")
	private String firstName;
    @NotBlank(message = "Last name is a mandatory field")
	private String lastName;
    @Min(value = 13, message = "Minimum age required for the customer is 13")
    @Max(value = 150, message = "Maximum age supported for the customer is 150")
    private int age;
    @NotBlank(message = "customer phone number is a mandatory field")
    @Pattern(regexp = "^[0-9]{10}$",message = "Please give a valid phone number")
	private String phoneNumber;
    @NotBlank(message = "customer email is a mandatory field")
    @Email
	private String email;    
    @NotBlank(message = "password is a mandatory field")
    private String password;
    
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public CustomerRegistrationDto() {
		super();		
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public CustomerRegistrationDto(String firstName,
			String lastName,
			int age,
			String phoneNumber,
			String email,
			String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.password = password;
	}
}
