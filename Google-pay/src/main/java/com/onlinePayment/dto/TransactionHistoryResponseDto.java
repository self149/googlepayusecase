package com.onlinePayment.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionHistoryResponseDto {
	
private Long transactionID;
	
	
	private String customerPhoneNumber;
	private String recepientPhoneNumber;
	private BigDecimal transactionAmount;
	private String transactionType;
	private Date transactionDate;
	private String comment;
	public Long getTransactionID() {
		return transactionID;
	}
	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}
	public String getRecepientPhoneNumber() {
		return recepientPhoneNumber;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public String getComment() {
		return comment;
	}
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}
	public void setRecepientPhoneNumber(String recepientPhoneNumber) {
		this.recepientPhoneNumber = recepientPhoneNumber;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public TransactionHistoryResponseDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TransactionHistoryResponseDto(Long transactionID, String customerPhoneNumber, String recepientPhoneNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comment) {
		this.transactionID = transactionID;
		this.customerPhoneNumber = customerPhoneNumber;
		this.recepientPhoneNumber = recepientPhoneNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comment = comment;
	}
	
	

}
