package com.onlinePayment.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;

public class TransferAmountWithPhoneNumberRequestDto {

	@Min(value = 1, message = "From Phone number is a mandatory field. Please give valid account number")
	private String fromPhoneNumber;
	@Min(value = 1, message = "to Phone number is a mandatory field. Please give valid account number")
	private String toPhoneNumber;
	@Min(value = 1, message = "Amount to be transferred is a mandatory field and should be of minimum 1 rupee")
	private BigDecimal amountToBeTransferred;
	private Date dateOfTransaction;
	private String comment;
	
	
	
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}
	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}
	public String getToPhoneNumber() {
		return toPhoneNumber;
	}
	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}
	public void setToPhoneNumber(String toPhoneNumber) {
		this.toPhoneNumber = toPhoneNumber;
	}
	public TransferAmountWithPhoneNumberRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TransferAmountWithPhoneNumberRequestDto(
			String fromPhoneNumber,
			String toPhoneNumber,
			BigDecimal amountToBeTransferred,
			Date dateOfTransaction, String comment) {
		super();
		this.fromPhoneNumber = fromPhoneNumber;
		this.toPhoneNumber = toPhoneNumber;
		this.amountToBeTransferred = amountToBeTransferred;
		this.dateOfTransaction = dateOfTransaction;
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "TransferAmountWithPhoneNumberRequestDto [fromPhoneNumber=" + fromPhoneNumber + ", toPhoneNumber="
				+ toPhoneNumber + ", amountToBeTransferred=" + amountToBeTransferred + ", dateOfTransaction="
				+ dateOfTransaction + ", comment=" + comment + "]";
	}
	
	
	
	

}
