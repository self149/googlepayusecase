package com.onlinePayment.controller;

import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinePayment.dto.TransactionSuccessResponseDto;
import com.onlinePayment.dto.TransferAmountWithPhoneNumberRequestDto;
import com.onlinePayment.model.TransactionHistory;
import com.onlinePayment.service.TransferAmountService;

@RestController
public class TransactionController {

	Logger logger = LoggerFactory.getLogger(TransactionController.class);
	@Autowired
	TransferAmountService transferAmountService;

	@PutMapping("/amountTransferThroughMobileNumbers")
	public ResponseEntity<TransactionSuccessResponseDto> transferMoneyThroughPhoneNumber(@Valid @RequestBody TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest) {
		logger.info("TransactionController.transferMoneyThroughPhoneNumber :: Entered the method successfully with request body: " + transferAmountWithPhoneNumberRequest);
		
		TransactionSuccessResponseDto transactionResponse = 
				transferAmountService.transferAmountThroughPhoneNumber(transferAmountWithPhoneNumberRequest);
		
		logger.info("TransactionController.transferMoneyThroughPhoneNumber :: Exiting the method with success response.");
		return ResponseEntity.status(HttpStatus.OK).body(transactionResponse);
	}
	
	@GetMapping("/mobileNumber/{mobileNumber}/transactionhistory")
	public ResponseEntity<List<TransactionHistory>> getLast10Transactions(@PathVariable String phoneNumber) {
		logger.info("TransactionController.getLast10Transactions :: Entered the method successfully with Phone Number: " + phoneNumber);
		// TODO: Imp: Change the response to TransactionHistoryResponseDto and verify the functionality
		List<TransactionHistory> transactionHistoryList = transferAmountService.getLatestTransactions(phoneNumber);
		
		logger.info("TransactionController.getLast10Transactions :: Exiting the method with success response.");
		return ResponseEntity.status(HttpStatus.OK).body(transactionHistoryList);		
	}
}
