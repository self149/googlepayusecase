package com.onlinePayment.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinePayment.dto.CustomerCreatedResponseDto;
import com.onlinePayment.dto.CustomerRegistrationDto;
import com.onlinePayment.service.CustomerService;

@RestController
public class CustomerController {
	
	Logger logger = LoggerFactory.getLogger(CustomerController.class);
	@Autowired
	CustomerService customerService;

	@PostMapping("/customerRegistration")
	public ResponseEntity<CustomerCreatedResponseDto> registerCustomer(@Valid @RequestBody CustomerRegistrationDto customerRegistrationDto) {
		logger.info("CustomerController.registerCustomer :: Entered the method successfully");
		CustomerCreatedResponseDto customerCreatedResponse = customerService.registerCustomer(customerRegistrationDto);
		logger.info("CustomerController.registerCustomer :: Exiting the method with success response");
		return ResponseEntity.status(HttpStatus.OK).body(customerCreatedResponse);
	}

}
