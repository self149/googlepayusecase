package com.onlinePayment.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class TransactionHistory {

	@Id @GeneratedValue
	private Long transactionID;
	
	
	private String customerPhoneNumber;
	
	private String recepientPhoneNumber;

	private BigDecimal transactionAmount;
	private String transactionType;
	@Column	
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date transactionDate = new Date();
	private String comment;
	
	public Long getTransactionID() {
		return transactionID;
	}
	
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public String getComment() {
		return comment;
	}
	public TransactionHistory setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
		return this;
	}
	
	public TransactionHistory setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
		return this;
	}
	public TransactionHistory setTransactionType(String transactionType) {
		this.transactionType = transactionType;
		return this;
	}
	public TransactionHistory setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
		return this;
	}
	public TransactionHistory setComment(String comment) {
		this.comment = comment;
		return this;
	}
	
	
	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}

	public String getRecepientPhoneNumber() {
		return recepientPhoneNumber;
	}

	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}

	public void setRecepientPhoneNumber(String recepientPhoneNumber) {
		this.recepientPhoneNumber = recepientPhoneNumber;
	}

	public TransactionHistory() {
		
	}

	public TransactionHistory(String customerPhoneNumber, String recepientPhoneNumber,
			BigDecimal transactionAmount, String transactionType, String comment) {
		super();		
		this.customerPhoneNumber = customerPhoneNumber;
		this.recepientPhoneNumber = recepientPhoneNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;		
		this.comment = comment;
	}

	public TransactionHistory(Long transactionID, String customerPhoneNumber, String recepientPhoneNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comment) {
		this.transactionID = transactionID;
		this.customerPhoneNumber = customerPhoneNumber;
		this.recepientPhoneNumber = recepientPhoneNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comment = comment;
	}

	
	
		
	
	
	
}
