package com.onlinePayment.service;

import com.onlinePayment.dto.CustomerCreatedResponseDto;
import com.onlinePayment.dto.CustomerRegistrationDto;

public interface CustomerService {

	CustomerCreatedResponseDto registerCustomer(CustomerRegistrationDto customerRegistrationDto);

}
