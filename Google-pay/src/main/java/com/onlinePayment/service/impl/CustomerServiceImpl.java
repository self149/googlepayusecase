package com.onlinePayment.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.onlinePayment.dto.CustomerCreatedResponseDto;
import com.onlinePayment.dto.CustomerRegistrationDto;
import com.onlinePayment.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.onlinePayment.exception.InvalidCustomerRegistrationDetailsException;
import com.onlinePayment.feignclient.UskBankClient;
import com.onlinePayment.model.Customer;
import com.onlinePayment.repository.CustomerRepository;
import com.onlinePayment.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	UskBankClient client;

	@Override
	public CustomerCreatedResponseDto registerCustomer(CustomerRegistrationDto customerRegistrationDto) {
		logger.info("CustomerServiceImpl.registerCustomer :: Entered the method successfully.");
		
		ResponseEntity<CustomerValidationWithPhoneNumberResponseDto> customerValidation = client.customerValidationWithPhoneNumber(customerRegistrationDto.getPhoneNumber());
		if(!customerValidation.getStatusCode().is2xxSuccessful())
			throw new InvalidCustomerRegistrationDetailsException("No phone number found with the given phone number: ", customerValidation);
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRegistrationDto, customer);
		customer = customerRepository.save(customer);
		
		logger.info("CustomerServiceImpl.registerCustomer :: Exiting the method successfully.");
		
		return new CustomerCreatedResponseDto(customer.getCustomerID(), customer.getFirstName(),
												customer.getLastName(), customer.getPhoneNumber());
	}

}
