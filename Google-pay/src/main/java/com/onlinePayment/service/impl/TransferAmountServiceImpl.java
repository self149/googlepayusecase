package com.onlinePayment.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.onlinePayment.dto.TransactionSuccessResponseDto;
import com.onlinePayment.dto.TransferAmountWithPhoneNumberRequestDto;
import com.onlinePayment.exception.CustomerNotRegisteredWithGpayAppException;
import com.onlinePayment.exception.TransferAmountException;
import com.onlinePayment.feignclient.UskBankClient;
import com.onlinePayment.model.Customer;
import com.onlinePayment.model.TransactionHistory;
import com.onlinePayment.repository.CustomerRepository;
import com.onlinePayment.repository.TransactionHistoryRepository;
import com.onlinePayment.service.TransferAmountService;

@Service
public class TransferAmountServiceImpl implements TransferAmountService {
	
	Logger logger = LoggerFactory.getLogger(TransferAmountServiceImpl.class);
	
	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	UskBankClient client;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public TransactionSuccessResponseDto transferAmountThroughPhoneNumber(TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest) {
		logger.info("TransferAmountServiceImpl.transferAmountThroughPhoneNumber :: Entered the method successfully.");
		validateIfThePhoneNumberIsRegisteredWithGpay(transferAmountWithPhoneNumberRequest);
		
		logger.info("TransferAmountServiceImpl.transferAmountThroughPhoneNumber :: calling Bank Service for transaction.");
		ResponseEntity<TransactionSuccessResponseDto> transactionSuccess = callBankServiceForAmountTransaction(
				transferAmountWithPhoneNumberRequest);
		if(!transactionSuccess.getStatusCode().is2xxSuccessful())
			throw new TransferAmountException(transactionSuccess);
	
		logger.info("TransferAmountServiceImpl.transferAmountThroughPhoneNumber :: Got Successful Response from Bank Service.");
		TransactionSuccessResponseDto transactionSuccessResponse = createAndSaveTransactionHistory(
				transferAmountWithPhoneNumberRequest);
		
		logger.info("TransferAmountServiceImpl.transferAmountThroughPhoneNumber :: Exiting the method successfully.");
		return transactionSuccessResponse;
	}

	@Override
	public List<TransactionHistory> getLatest10Transactions(String customerPhoneNumber) {		
		logger.info("TransferAmountServiceImpl.getLatest10Transactions :: Entered the method successfully.");
		
		List<TransactionHistory> transactionHistoryList = transactionHistoryRepository.findFirst10ByCustomerPhoneNumberOrderByTransactionDateDesc(customerPhoneNumber);
		
		logger.info("TransferAmountServiceImpl.getLatest10Transactions :: Exiting the method successfully.");
		return transactionHistoryList;
	}	
	
	@Override
	public List<TransactionHistory> getLatestTransactions(String customerPhoneNumber) {
		logger.info("TransferAmountServiceImpl.getLatestTransactions :: Entered the method successfully.");
		Pageable pageable = PageRequest.of(0, 10);
		List<TransactionHistory> transactionHistoryList = 
				transactionHistoryRepository.findByCustomerPhoneNumberOrderByTransactionDateDesc(customerPhoneNumber, pageable);
		
		logger.info("TransferAmountServiceImpl.getLatestTransactions :: Exiting the method successfully.");
		return transactionHistoryList;
	}
	
	private ResponseEntity<TransactionSuccessResponseDto> callBankServiceForAmountTransaction(
			TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest) {
		ResponseEntity<TransactionSuccessResponseDto> transactionSuccess =
				client.transferMoneyThroughPhoneNumber(transferAmountWithPhoneNumberRequest);
		return transactionSuccess;
	}

	private TransactionSuccessResponseDto createAndSaveTransactionHistory(
			TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest) {
		TransactionHistory debitTransaction = new TransactionHistory(transferAmountWithPhoneNumberRequest.getFromPhoneNumber(), 
															transferAmountWithPhoneNumberRequest.getToPhoneNumber(), 
															transferAmountWithPhoneNumberRequest.getAmountToBeTransferred(),
															"DEBIT",
															transferAmountWithPhoneNumberRequest.getComment());
		TransactionHistory creditTransaction = new TransactionHistory( 
				transferAmountWithPhoneNumberRequest.getToPhoneNumber(),
				transferAmountWithPhoneNumberRequest.getFromPhoneNumber(),
				transferAmountWithPhoneNumberRequest.getAmountToBeTransferred(),
				"CREDIT",
				transferAmountWithPhoneNumberRequest.getComment());
		
		debitTransaction =  transactionHistoryRepository.save(debitTransaction);
		creditTransaction = transactionHistoryRepository.save(creditTransaction);
		TransactionSuccessResponseDto transactionSuccessResponse = new TransactionSuccessResponseDto(debitTransaction.getTransactionID(), 
																			creditTransaction.getTransactionID());
		return transactionSuccessResponse;
	}
	
	private void validateIfThePhoneNumberIsRegisteredWithGpay(
			TransferAmountWithPhoneNumberRequestDto transferAmountRequest) {
		logger.info("TransferAmountServiceImpl.validateIfThePhoneNumberIsRegisteredWithGpay :: Entered the method successfully.");
		List<String> phoneNumbers = new ArrayList<>(
				Arrays.asList(transferAmountRequest.getFromPhoneNumber(),transferAmountRequest.getToPhoneNumber()));
		
		List<Customer> customerList = customerRepository.findByPhoneNumberIn(phoneNumbers);
		
		if(customerList.size()<2) {
			logger.error("TransferAmountServiceImpl.validateIfThePhoneNumberIsRegisteredWithGpay :: One or more customers are not registered with the phone numbers in GPAY Application.");
			throw new CustomerNotRegisteredWithGpayAppException("Customer Registration with Gpay is mandatory for transferring the amount");			
		}
		
		logger.info("TransferAmountServiceImpl.validateIfThePhoneNumberIsRegisteredWithGpay :: Exiting the method successfully.");
	}
}
