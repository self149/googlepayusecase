package com.onlinePayment.service;

import java.util.List;

import com.onlinePayment.dto.TransactionSuccessResponseDto;
import com.onlinePayment.dto.TransferAmountWithPhoneNumberRequestDto;
import com.onlinePayment.model.TransactionHistory;

public interface TransferAmountService {
	public TransactionSuccessResponseDto transferAmountThroughPhoneNumber(
								TransferAmountWithPhoneNumberRequestDto transferAmountWithPhoneNumberRequest);
	
	public List<TransactionHistory> getLatest10Transactions(String customerPhoneNumber);
	public List<TransactionHistory> getLatestTransactions(String customerPhoneNumber);
}
