package com.onlinePayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinePayment.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	public List<Customer> findByPhoneNumberIn(List<String> phoneNumbers);

}
