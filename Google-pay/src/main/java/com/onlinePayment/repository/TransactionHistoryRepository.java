package com.onlinePayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinePayment.model.TransactionHistory;

import org.springframework.data.domain.Pageable;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Long> {	
		public List<TransactionHistory> findFirst10ByCustomerPhoneNumberOrderByTransactionDateDesc(String customerPhoneNumber);
		
		public List<TransactionHistory> findByCustomerPhoneNumberOrderByTransactionDateDesc(String customerPhoneNumber, Pageable pageable);

}
