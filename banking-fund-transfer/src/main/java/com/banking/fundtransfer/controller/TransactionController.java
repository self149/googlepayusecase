package com.banking.fundtransfer.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.fundtransfer.dto.TransactionHistoryResponseDto;
import com.banking.fundtransfer.dto.TransactionSuccessResponse;
import com.banking.fundtransfer.dto.TransferAmountWithPhoneNumberRequestDto;
import com.banking.fundtransfer.dto.TransferBalanceRequestDto;
import com.banking.fundtransfer.exception.AccountException;
import com.banking.fundtransfer.exception.AccountNotFoundException;
import com.banking.fundtransfer.service.AccountService;
import com.banking.fundtransfer.service.TransactionService;

@RestController
public class TransactionController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	TransactionService transactionService;
	
	@GetMapping("/monthlyStatement/account-number/{accountNumber}/month/{month}/year/{year}")
	public ResponseEntity<List<TransactionHistoryResponseDto>> getMonthlyStatementOfTheGivenAccount(@PathVariable @NotBlank @NotEmpty Long accountNumber, @NotBlank @NotEmpty String month, @NotBlank @NotEmpty String year) {
		
		List<TransactionHistoryResponseDto> monthlyStatement = transactionService.getStatementOfAccount(accountNumber, month, year);		
		return ResponseEntity.ok().header("desc", "retrieved monthly statement for the account of the given month").body(monthlyStatement);
	}
	
	@PutMapping("/amountTransfer")
	public ResponseEntity<TransactionSuccessResponse> transferAmount(@Valid @RequestBody TransferBalanceRequestDto transferBalanceRequest) throws AccountException, AccountNotFoundException {		
		TransactionSuccessResponse transactionSuccessResponse = transactionService.transferAmount(transferBalanceRequest);
		HttpHeaders header = new HttpHeaders();
		header.add("desc", "amount transferred successfully");
		return ResponseEntity.status(HttpStatus.OK).headers(header).body(transactionSuccessResponse);
	}
	
	@PutMapping("/amountTransferByPhoneNumber")
	public ResponseEntity<TransactionSuccessResponse> transferAmountByPhoneNumber(@Valid @RequestBody TransferAmountWithPhoneNumberRequestDto transferBalanceRequest) throws AccountException, AccountNotFoundException {		
		TransactionSuccessResponse transactionSuccessResponse = transactionService.transferAmountWithPhoneNumber(transferBalanceRequest);
		HttpHeaders header = new HttpHeaders();
		header.add("desc", "amount transferred successfully");
		return ResponseEntity.status(HttpStatus.OK).headers(header).body(transactionSuccessResponse);
	}
	
	@GetMapping("/checkAccountBalance/{accountNumber}")
	public ResponseEntity<BigDecimal> getAccountBalance(@PathVariable @NotBlank @NotEmpty Long accountNumber) throws AccountNotFoundException  {
		BigDecimal accountBalance = accountService.getAccountBalance(accountNumber);
		return ResponseEntity.status(HttpStatus.OK).body(accountBalance);
	}
	
}

