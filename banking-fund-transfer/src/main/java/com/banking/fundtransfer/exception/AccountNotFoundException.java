package com.banking.fundtransfer.exception;

public class AccountNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountNotFoundException(String errorMessage) {
		super(errorMessage);
	}

	
}
