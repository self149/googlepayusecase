package com.banking.fundtransfer.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class TransactionHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long transactionID;
	
	
	private Long customerAccountNumber;
	
	private Long otherAccountNumber;

	private BigDecimal transactionAmount;
	
	private String transactionType;
	
	@Column	
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern="yyyy-MM-dd")
	private Date transactionDate;
	
	private String comment;
	
	public Long getTransactionID() {
		return transactionID;
	}
	public Long getCustomerAccountNumber() {
		return customerAccountNumber;
	}
	public Long getOtherAccountNumber() {
		return otherAccountNumber;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public String getComment() {
		return comment;
	}
	public TransactionHistory setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
		return this;
	}
	public TransactionHistory setCustomerAccountNumber(Long customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
		return this;
	}
	public TransactionHistory setOtherAccountNumber(Long otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
		return this;
	}
	public TransactionHistory setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
		return this;
	}
	public TransactionHistory setTransactionType(String transactionType) {
		this.transactionType = transactionType;
		return this;
	}
	public TransactionHistory setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
		return this;
	}
	public TransactionHistory setComment(String comment) {
		this.comment = comment;
		return this;
	}
	public TransactionHistory(Long transactionID, Long customerAccountNumber, Long otherAccountNumber,
			BigDecimal transactionAmount, String transactionType, Date transactionDate, String comment) {
		super();
		this.transactionID = transactionID;
		this.customerAccountNumber = customerAccountNumber;
		this.otherAccountNumber = otherAccountNumber;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
		this.transactionDate = transactionDate;
		this.comment = comment;
	}
	
	public TransactionHistory() {
		
	}
	
		
	
	
}
