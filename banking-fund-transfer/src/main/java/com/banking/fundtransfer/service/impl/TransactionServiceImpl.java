package com.banking.fundtransfer.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banking.fundtransfer.dto.TransactionHistoryResponseDto;
import com.banking.fundtransfer.dto.TransactionSuccessResponse;
import com.banking.fundtransfer.dto.TransferAmountWithPhoneNumberRequestDto;
import com.banking.fundtransfer.dto.TransferBalanceRequestDto;
import com.banking.fundtransfer.exception.AccountException;
import com.banking.fundtransfer.exception.AccountNotFoundException;
import com.banking.fundtransfer.model.Account;
import com.banking.fundtransfer.model.TransactionHistory;
import com.banking.fundtransfer.repository.AccountRepository;
import com.banking.fundtransfer.repository.TransactionHistoryRepository;
import com.banking.fundtransfer.service.TransactionService;

@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

	private static final String PHONE_NUMBER_TRANSACTION = "Transaction Via Phone Number: ";

	@Autowired
	TransactionHistoryRepository transactionRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	public TransactionSuccessResponse transferAmount(TransferBalanceRequestDto transferBalanceRequest) throws AccountException, AccountNotFoundException{
		Long fromAccountNumber = transferBalanceRequest.getFromAccountNumber();
		Long toAccountNumber = transferBalanceRequest.getToAccountNumber();
		BigDecimal amountToBeTransferred = transferBalanceRequest.getAmountToBeTransferred();
		
		Account fromAccount = accountRepository.findById(fromAccountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Such account exists with the given account number: " + fromAccountNumber));
		Account toAccount = accountRepository.findById(toAccountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Such account exists with the given account number: " + toAccountNumber));
		
		processAmountTransfer(amountToBeTransferred, fromAccount, toAccount);
		
		return createTransactionHistory(fromAccountNumber, toAccountNumber, amountToBeTransferred, transferBalanceRequest.getDateOfTransaction(),transferBalanceRequest.getComment());
		
		
	}

	public List<TransactionHistoryResponseDto> getStatementOfAccount(Long accountNumber, String month, String year) {
		List<TransactionHistory> transactionHistoryList = transactionRepository.getStatementOfAccount(accountNumber,Integer.valueOf(month),Integer.valueOf(year));
		return transactionHistoryList.stream().map(transactionHistory -> {
			TransactionHistoryResponseDto transactionHistoryDto = new TransactionHistoryResponseDto();
			BeanUtils.copyProperties(transactionHistory, transactionHistoryDto);
			return transactionHistoryDto;
		}).collect(Collectors.toList());
	}
	

	@Override
	public TransactionSuccessResponse transferAmountWithPhoneNumber(
			TransferAmountWithPhoneNumberRequestDto transferAmountRequest) {
		String fromPhoneNumber = transferAmountRequest.getFromPhoneNumber();
		String toPhoneNumber = transferAmountRequest.getToPhoneNumber();
		BigDecimal amountToBeTransferred = transferAmountRequest.getAmountToBeTransferred();
		
		Account fromAccount = accountRepository.findByCustomer_phoneNumber(fromPhoneNumber).
				orElseThrow(() -> new AccountNotFoundException("No Such account exists with the given phone number: " + fromPhoneNumber));
		Account toAccount = accountRepository.findByCustomer_phoneNumber(toPhoneNumber).
				orElseThrow(() -> new AccountNotFoundException("No Such account exists with the given phone number: " + toPhoneNumber));
		
		processAmountTransfer(amountToBeTransferred, fromAccount, toAccount);
		
		return createTransactionHistory(fromAccount.getAccountNumber(), toAccount.getAccountNumber(), amountToBeTransferred, 
					transferAmountRequest.getDateOfTransaction(),PHONE_NUMBER_TRANSACTION + transferAmountRequest.getComment());

	}
	
	private TransactionSuccessResponse createTransactionHistory(Long fromAccountNumber,
			Long toAccountNumber, BigDecimal amountToBeTransferred, Date dateOfTransaction, String comment) {
		TransactionHistory debitTransaction = new TransactionHistory(0L, fromAccountNumber, toAccountNumber, amountToBeTransferred, 
																		"DEBIT", dateOfTransaction, comment);
		debitTransaction = transactionRepository.save(debitTransaction);
		
		TransactionHistory creditTransaction = new TransactionHistory(0L, toAccountNumber, fromAccountNumber, amountToBeTransferred, 
																		"CREDIT", dateOfTransaction, comment);
		creditTransaction = transactionRepository.save(creditTransaction);
		
		TransactionSuccessResponse transactionResponse = new TransactionSuccessResponse(debitTransaction.getTransactionID(), 
																	creditTransaction.getTransactionID());
		return transactionResponse;
	}
	
	private void processAmountTransfer(BigDecimal amountToBeTransferred, Account fromAccount, Account toAccount) {
		if(fromAccount.getCurrentBalance().compareTo(amountToBeTransferred) < 0) {
			throw new AccountException("Your account dosen't have enough balance to make this transaction");		
		}
		
		fromAccount.setCurrentBalance(fromAccount.getCurrentBalance().subtract(amountToBeTransferred));
		accountRepository.save(fromAccount);
		toAccount.setCurrentBalance(toAccount.getCurrentBalance().add(amountToBeTransferred));		
		accountRepository.save(toAccount);
	}
}
