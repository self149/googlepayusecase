package com.banking.fundtransfer.service;

import java.math.BigDecimal;
import java.util.List;

import com.banking.fundtransfer.exception.AccountNotFoundException;
import com.banking.fundtransfer.model.Account;

public interface AccountService {
	
	public List<Account> getAllAccounts();
	public BigDecimal getAccountBalance(Long accountNumber) throws AccountNotFoundException;

}
