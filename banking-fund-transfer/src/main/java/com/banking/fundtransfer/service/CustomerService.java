package com.banking.fundtransfer.service;

import com.banking.fundtransfer.dto.CustomerRegistrationDto;
import com.banking.fundtransfer.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.banking.fundtransfer.model.Customer;

public interface CustomerService {
	
public Customer registerNewCustomer(CustomerRegistrationDto customerRegistrationDto);

public CustomerValidationWithPhoneNumberResponseDto validateCustomerByPhoneNumber(String phoneNumber);

}
