package com.banking.fundtransfer.service.impl;

import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banking.fundtransfer.dto.CustomerRegistrationDto;
import com.banking.fundtransfer.dto.CustomerValidationWithPhoneNumberResponseDto;
import com.banking.fundtransfer.exception.CustomerNotFoundException;
import com.banking.fundtransfer.model.Account;
import com.banking.fundtransfer.model.Customer;
import com.banking.fundtransfer.model.TransactionHistory;
import com.banking.fundtransfer.repository.AccountRepository;
import com.banking.fundtransfer.repository.CustomerRepository;
import com.banking.fundtransfer.repository.TransactionHistoryRepository;
import com.banking.fundtransfer.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	TransactionHistoryRepository transactionRepository;
	
	@Transactional
	public Customer registerNewCustomer(CustomerRegistrationDto customerRegistrationDto) {
		
		Customer customer = new Customer();
		BeanUtils.copyProperties(customerRegistrationDto, customer);
		
		Account account = new Account();
		BeanUtils.copyProperties(customerRegistrationDto.getAccountRegistrationDto(), account);
		account.setCustomer(customer);
		account = accountRepository.save(account);
		
		customer = customerRepository.save(customer);
		TransactionHistory initialDeposit = new TransactionHistory(0L, account.getAccountNumber(), account.getAccountNumber(), account.getCurrentBalance(), 
				"Account Opening Initial Deposit", new Date(), "Deposit while opening the account");
			transactionRepository.save(initialDeposit);
		
		return customer;
	}

	@Override
	public CustomerValidationWithPhoneNumberResponseDto validateCustomerByPhoneNumber(String phoneNumber) {
		Customer customer = customerRepository.findByPhoneNumber(phoneNumber).orElseThrow( () -> new CustomerNotFoundException("No Customer found with the given phone number"));
		
		CustomerValidationWithPhoneNumberResponseDto customerValidationResponse = new CustomerValidationWithPhoneNumberResponseDto();
		
		Boolean isCustomerFound = customer.getPhoneNumber().equals(phoneNumber) ? true : false;
		customerValidationResponse.setDoesCustomerExists(isCustomerFound);
	
		return customerValidationResponse;
	}
	

}
