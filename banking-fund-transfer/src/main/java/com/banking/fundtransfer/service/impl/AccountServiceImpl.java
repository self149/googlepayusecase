package com.banking.fundtransfer.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.banking.fundtransfer.exception.AccountNotFoundException;
import com.banking.fundtransfer.model.Account;
import com.banking.fundtransfer.repository.AccountRepository;
import com.banking.fundtransfer.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	AccountRepository accountRepository;
	
	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}
	
	public BigDecimal getAccountBalance(Long accountNumber) throws AccountNotFoundException {
		Account account = accountRepository.findById(accountNumber).
				orElseThrow(() -> new AccountNotFoundException("No Such account exists with the given account number: " + accountNumber));
		return account.getCurrentBalance(); 
	}
	
}
