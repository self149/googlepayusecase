package com.banking.fundtransfer.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class AccountRegistrationDto {
	
	@NotBlank(message = "Please enter the Bank name")
	private String bankName;
	
	@NotBlank(message = "Please enter the Type of Account")
	private String accountType;
	
	@Min(1000)
	private BigDecimal currentBalance;
	
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	
	
	

}
