package com.banking.fundtransfer.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CustomerRegistrationDto {
	
    @NotBlank(message = "Please enter the First Name")
	private String firstName;
    
    @NotBlank(message = "Please enter the Last Name")
	private String lastName;
    
    @Min(value = 18, message = "The Minimum age required for the customer is 18")
    @Max(value = 100, message = "The Maximum age supported for the customer is 100")
    private int age;
    
    @NotBlank(message = "Please enter the valid Address of Customer")
    private String address;
    
    @NotBlank(message = "Please enter the valid Mobile Number")
	private String phoneNumber;
    
    @NotBlank(message = "Please enter the valid E-Mail Address")
    @Email
	private String email;  
    
    @NotBlank(message = "Please enter the valid UAN Number")
	private String uanNumber;
    
    @NotBlank(message = "Please enter the valid Password")
    private String password;
    
    @Valid	
	private AccountRegistrationDto accountRegistrationDto;
	
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	public String getAddress() {
		return address;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public AccountRegistrationDto getAccountRegistrationDto() {
		return accountRegistrationDto;
	}
	public void setAccountRegistrationDto(AccountRegistrationDto accountRegistrationDto) {
		this.accountRegistrationDto = accountRegistrationDto;
	}
	public CustomerRegistrationDto() {
		super();		
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUanNumber() {
		return uanNumber;
	}
	public void setUanNumber(String uanNumber) {
		this.uanNumber = uanNumber;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
