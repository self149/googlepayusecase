package com.banking.fundtransfer.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class TransactionHistoryResponseDto {

	private Long transactionID;
	
	private Long customerAccountNumber;
	
	private Long otherAccountNumber;
	
	private BigDecimal transactionAmount;
	
	private String transactionType;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date transactionDate;
	
	private String comment;
	
	public Long getTransactionID() {
		return transactionID;
	}
	public Long getCustomerAccountNumber() {
		return customerAccountNumber;
	}
	public Long getOtherAccountNumber() {
		return otherAccountNumber;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public String getComment() {
		return comment;
	}
	public void setTransactionID(Long transactionID) {
		this.transactionID = transactionID;
	}
	public void setCustomerAccountNumber(Long customerAccountNumber) {
		this.customerAccountNumber = customerAccountNumber;
	}
	public void setOtherAccountNumber(Long otherAccountNumber) {
		this.otherAccountNumber = otherAccountNumber;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	

}
