package com.banking.fundtransfer.dto;

public class TransactionSuccessResponse {
	
	Long debitTransactionID;
	
	Long creditTransactionID;
	
	public TransactionSuccessResponse(Long debitTransactionID, Long creditTransactionID) {
		super();
		this.debitTransactionID = debitTransactionID;
		this.creditTransactionID = creditTransactionID;
	}
	public Long getDebitTransactionID() {
		return debitTransactionID;
	}
	public Long getCreditTransactionID() {
		return creditTransactionID;
	}
	public void setDebitTransactionID(Long debitTransactionID) {
		this.debitTransactionID = debitTransactionID;
	}
	public void setCreditTransactionID(Long creditTransactionID) {
		this.creditTransactionID = creditTransactionID;
	}
	
	

}
