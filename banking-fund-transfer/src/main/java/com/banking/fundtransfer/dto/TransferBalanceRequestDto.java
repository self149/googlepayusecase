package com.banking.fundtransfer.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Min;

public class TransferBalanceRequestDto {

	@Min(value = 1, message = "Please enter the Amount of From Account number")
	private Long fromAccountNumber;
	
	@Min(value = 1, message = "Please enter the Amount of To Account number")
	private Long toAccountNumber;
	
	@Min(value = 1, message = "Amount to be transferred is a mandatory field and should be of minimum 1 rupee")
	private BigDecimal amountToBeTransferred;
	
	private Date dateOfTransaction;
	
	private String comment;
	
	
	
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Date getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(Date dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	public Long getFromAccountNumber() {
		return fromAccountNumber;
	}
	public void setFromAccountNumber(Long fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}
	public Long getToAccountNumber() {
		return toAccountNumber;
	}
	public void setToAccountNumber(Long toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}
	public BigDecimal getAmountToBeTransferred() {
		return amountToBeTransferred;
	}
	public void setAmountToBeTransferred(BigDecimal amountToBeTransferred) {
		this.amountToBeTransferred = amountToBeTransferred;
	}
	
	

}
