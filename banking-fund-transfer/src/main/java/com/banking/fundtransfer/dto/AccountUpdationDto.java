package com.banking.fundtransfer.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AccountUpdationDto {
	
	@Min(1)
	@NotNull(message = "Please enter the valid Account Number")
	private long accountNumber;
	
	@Min(1)
	@NotNull(message = "please specify the Amount to be Transferred")
	private BigDecimal addMoneyToAccount;
	
	
	public long getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getAddMoneyToAccount() {
		return addMoneyToAccount;
	}
	public void setAddMoneyToAccount(BigDecimal addMoneyToAccount) {
		this.addMoneyToAccount = addMoneyToAccount;
	}
	
	
}
