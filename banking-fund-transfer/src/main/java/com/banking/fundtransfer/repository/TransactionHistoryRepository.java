package com.banking.fundtransfer.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.banking.fundtransfer.model.TransactionHistory;

@Repository
public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, Long> {
	
	
	public List<TransactionHistory> findByCustomerAccountNumber(Long customerAccountNumber);
	
	@Query("select t from TransactionHistory t where t.customerAccountNumber = ?1 and  month(t.transactionDate) = ?2 and year(t.transactionDate) = ?3")
	public List<TransactionHistory> getStatementOfAccount(Long accountNumber, int month, int year);
	
}
