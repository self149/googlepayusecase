package com.banking.fundtransfer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.banking.fundtransfer.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	public Optional<Customer> findByPhoneNumber(String phoneNumber);

}
